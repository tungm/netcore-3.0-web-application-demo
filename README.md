# It's not finished yet! :(

![Project summary](./RepoItems/projec-sumary.jpg)


# Welcome to {project name}, a sample blog engine using .Net Core 3.0!

{Overview about project purpose and project structure.}

## Getting Started

{What things you need to install before build or run this project?}

### Visual Studio

{Steps to build and run this project on Visual Studio}

### Visual Studio Code

{Steps to build and run this project on Visual Studio Code}


## Solution Structure

{Solution structure and purpose of each component}

## Deployment

{How to deploy this project}

## Built With

{Which framework/library this project uses?}
