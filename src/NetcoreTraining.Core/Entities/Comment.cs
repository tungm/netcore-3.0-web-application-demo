﻿namespace NetcoreTraining.Core.Entities
{
    public class Comment : EntityBase
    {
        public string Content { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
