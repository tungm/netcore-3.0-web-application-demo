﻿using System.Collections.Generic;

namespace NetcoreTraining.Core.Entities
{
    public class Category : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Post> Posts { get; set; }
    }
}
