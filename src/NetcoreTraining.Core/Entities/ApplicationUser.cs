﻿using Microsoft.AspNetCore.Identity;

namespace NetcoreTraining.Core.Entities
{
    public class ApplicationUser : IdentityUser
    {
    }
}
