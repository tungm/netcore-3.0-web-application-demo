﻿using System.Collections.Generic;

namespace NetcoreTraining.Core.Entities
{
    public class Post : EntityBase
    {
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string Slug { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
