﻿using NetcoreTraining.Core.Entities;
using NetcoreTraining.Core.Enums;
using NetcoreTraining.Core.Interfaces.Repositories;
using NetcoreTraining.Core.Interfaces.Sevices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetcoreTraining.Core.Service
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public async Task<IEnumerable<Post>> ListPostByPageAsync(int pageSize, int pageNumber)
        {
            return await _postRepository.ListLatestPostByPageAsync(pageSize, pageNumber);
        }

        public async Task<Post> GetPostDetailById(int id)
        {
            return await _postRepository.GetDetailByIdAsync(id);
        }
    }
}
