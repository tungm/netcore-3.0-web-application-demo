﻿namespace NetcoreTraining.Core.Enums
{
    public enum SortOrder
    {
        Ascending,
        Descending
    }
}
