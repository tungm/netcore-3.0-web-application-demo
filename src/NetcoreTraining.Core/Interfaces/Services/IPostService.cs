﻿using NetcoreTraining.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetcoreTraining.Core.Interfaces.Sevices
{
    public interface IPostService
    {
        Task<IEnumerable<Post>> ListPostByPageAsync(int pageSize, int pageNumber);
        Task<Post> GetPostDetailById(int id);
    }
}
