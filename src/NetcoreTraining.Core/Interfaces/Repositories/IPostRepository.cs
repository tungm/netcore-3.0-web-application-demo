﻿using NetcoreTraining.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetcoreTraining.Core.Interfaces.Repositories
{
    public interface IPostRepository : IRepository<Post>
    {
        Task<IEnumerable<Post>> ListLatestPostByPageAsync(int pageSize, int pageNumber);

        Task<Post> GetDetailByIdAsync(int Id);
    }
}
