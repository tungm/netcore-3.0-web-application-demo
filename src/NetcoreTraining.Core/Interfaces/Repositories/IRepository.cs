﻿using NetcoreTraining.Core.Entities;
using NetcoreTraining.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NetcoreTraining.Core.Interfaces.Repositories
{
    public interface IRepository<T> where T : EntityBase
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetByCriteriaAsync(Expression<Func<T, bool>> criteria = null, Expression<Func<T, object>> orderBy = null,
            SortOrder sort = SortOrder.Ascending, int take = -1, int skip = -1);
        Task<T> GetByIdAsync(int id);
        Task<int> CountAsync();
        Task<int> CountByCriteriaAsync(Expression<Func<T, bool>> criteria);
    }
}
