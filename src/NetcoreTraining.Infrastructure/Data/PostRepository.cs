﻿using Microsoft.EntityFrameworkCore;
using NetcoreTraining.Core.Entities;
using NetcoreTraining.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetcoreTraining.Infrastructure.Data
{
    public class PostRepository : EfRepository<Post>, IPostRepository
    {
        public PostRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Post> GetDetailByIdAsync(int id)
        {
            return await _dbContext.Posts
                .Include(o => o.Category)
                .Include(o => o.Comments)
                .FirstOrDefaultAsync(o => o.Id == id);
        }

        public async Task<IEnumerable<Post>> ListLatestPostByPageAsync(int pageSize, int pageNumber)
        {
            return await _dbContext.Posts
                .Include(o => o.Category)
                .OrderByDescending(o => o.CreatedAt).Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToListAsync();
        }
    }
}
