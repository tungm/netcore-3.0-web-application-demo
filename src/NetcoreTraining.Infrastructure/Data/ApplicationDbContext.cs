﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NetcoreTraining.Core.Entities;
using System.Reflection;

namespace NetcoreTraining.Infrastructure.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .ToTable("Users");

            builder.Entity("Microsoft.AspNetCore.Identity.IdentityRole")
                .ToTable("Roles");

            builder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>")
               .ToTable("RoleClaims");

            builder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>")
               .ToTable("UserClaim");

            builder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>")
               .ToTable("UserLogin");

            builder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>")
               .ToTable("UserRole");

            builder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>")
               .ToTable("UserToken");

            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly()); // TODO: Not sure if it working or not, let check later
        }
    }
}
