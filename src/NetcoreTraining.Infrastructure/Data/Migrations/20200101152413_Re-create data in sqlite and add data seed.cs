﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NetcoreTraining.Infrastructure.Migrations
{
    public partial class Recreatedatainsqliteandadddataseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true, defaultValue: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceCodes",
                columns: table => new
                {
                    UserCode = table.Column<string>(maxLength: 200, nullable: false),
                    DeviceCode = table.Column<string>(maxLength: 200, nullable: false),
                    SubjectId = table.Column<string>(maxLength: 200, nullable: true),
                    ClientId = table.Column<string>(maxLength: 200, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    Expiration = table.Column<DateTime>(nullable: false),
                    Data = table.Column<string>(maxLength: 50000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceCodes", x => x.UserCode);
                });

            migrationBuilder.CreateTable(
                name: "PersistedGrants",
                columns: table => new
                {
                    Key = table.Column<string>(maxLength: 200, nullable: false),
                    Type = table.Column<string>(maxLength: 50, nullable: false),
                    SubjectId = table.Column<string>(maxLength: 200, nullable: true),
                    ClientId = table.Column<string>(maxLength: 200, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    Expiration = table.Column<DateTime>(nullable: true),
                    Data = table.Column<string>(maxLength: 50000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersistedGrants", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true, defaultValue: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<Guid>(nullable: true),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Thumbnail = table.Column<string>(nullable: true),
                    Slug = table.Column<string>(nullable: true),
                    ShortDescription = table.Column<string>(maxLength: 500, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaim_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogin",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogin", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogin_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRole_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserToken",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserToken", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserToken_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true, defaultValue: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DeletedBy = table.Column<Guid>(nullable: true),
                    Content = table.Column<string>(maxLength: 300, nullable: false),
                    PostId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "Name", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2020, 1, 1, 15, 24, 13, 244, DateTimeKind.Utc).AddTicks(1029), null, null, null, "Articles about programing", "Programing", null, null });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "Name", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 2, new DateTime(2020, 1, 1, 15, 24, 13, 244, DateTimeKind.Utc).AddTicks(2427), null, null, null, "Articles about traveling", "Travel", null, null });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "Name", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 3, new DateTime(2020, 1, 1, 15, 24, 13, 244, DateTimeKind.Utc).AddTicks(2451), null, null, null, "Articles about photography", "Photography", null, null });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Content", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "ShortDescription", "Slug", "Thumbnail", "Title", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 1, 1, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.", new DateTime(2020, 1, 1, 15, 24, 13, 251, DateTimeKind.Utc).AddTicks(1675), null, null, null, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.", null, "/images/post-thumb-1.jpg", "Lorem ipsum dolor sit amet", null, null });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Content", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "ShortDescription", "Slug", "Thumbnail", "Title", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 2, 2, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.", new DateTime(2020, 1, 1, 15, 24, 13, 251, DateTimeKind.Utc).AddTicks(3111), null, null, null, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.", null, "/images/post-thumb-1.jpg", "consetetur sadipscing elitr", null, null });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Content", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "ShortDescription", "Slug", "Thumbnail", "Title", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 3, 3, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.", new DateTime(2020, 1, 1, 15, 24, 13, 251, DateTimeKind.Utc).AddTicks(3143), null, null, null, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.", null, "/images/post-thumb-1.jpg", "sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat", null, null });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "PostId", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 1, "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren", new DateTime(2020, 1, 1, 15, 24, 13, 249, DateTimeKind.Utc).AddTicks(2129), null, null, null, 1, null, null });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "PostId", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 2, "Consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren", new DateTime(2020, 1, 1, 15, 24, 13, 249, DateTimeKind.Utc).AddTicks(2180), null, null, null, 1, null, null });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedAt", "CreatedBy", "DeletedAt", "DeletedBy", "PostId", "UpdatedAt", "UpdatedBy" },
                values: new object[] { 3, "Sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren", new DateTime(2020, 1, 1, 15, 24, 13, 249, DateTimeKind.Utc).AddTicks(2183), null, null, null, 1, null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceCodes_DeviceCode",
                table: "DeviceCodes",
                column: "DeviceCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DeviceCodes_Expiration",
                table: "DeviceCodes",
                column: "Expiration");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_Expiration",
                table: "PersistedGrants",
                column: "Expiration");

            migrationBuilder.CreateIndex(
                name: "IX_PersistedGrants_SubjectId_ClientId_Type",
                table: "PersistedGrants",
                columns: new[] { "SubjectId", "ClientId", "Type" });

            migrationBuilder.CreateIndex(
                name: "IX_Posts_CategoryId",
                table: "Posts",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaims_RoleId",
                table: "RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId",
                table: "UserClaim",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogin_UserId",
                table: "UserLogin",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RoleId",
                table: "UserRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "DeviceCodes");

            migrationBuilder.DropTable(
                name: "PersistedGrants");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "UserClaim");

            migrationBuilder.DropTable(
                name: "UserLogin");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "UserToken");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
