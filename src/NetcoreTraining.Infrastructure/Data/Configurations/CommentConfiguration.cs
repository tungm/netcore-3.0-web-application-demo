﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetcoreTraining.Core.Entities;
using System;

namespace NetcoreTraining.Infrastructure.Data.Configurations
{
    internal class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CreatedAt)
                .HasDefaultValueSql("GETDATE()");

            builder.Property(entity => entity.IsDeleted)
                .HasDefaultValue(false);

            builder.HasOne(comment => comment.Post)
                .WithMany(post => post.Comments)
                .HasForeignKey(comment => comment.PostId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(comment => comment.PostId);

            builder.Property(comment => comment.Content)
                .IsRequired()
                .HasMaxLength(300);

            builder.HasData(new Comment[]
            {
                new Comment
                {
                    Id = 1,
                    PostId = 1,
                    Content = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren",
                    CreatedAt = DateTime.UtcNow
                },
                new Comment
                {
                    Id = 2,
                    PostId = 1,
                    Content = "Consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren",
                    CreatedAt = DateTime.UtcNow
                },
                new Comment
                {
                    Id = 3,
                    PostId = 1,
                    Content = "Sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren",
                    CreatedAt = DateTime.UtcNow
                }
            });
        }
    }
}
