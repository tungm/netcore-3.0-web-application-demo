﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetcoreTraining.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetcoreTraining.Infrastructure.Data.Configurations
{
    internal class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CreatedAt)
                .HasDefaultValueSql("GETDATE()");

            builder.Property(entity => entity.IsDeleted)
                .HasDefaultValue(false);

            builder.Property(category => category.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.HasData(new Category[]
            {
                new Category
                {
                    Id = 1,
                    Name = "Programing",
                    CreatedAt = DateTime.UtcNow,
                    Description = "Articles about programing",
                },
                new Category
                {
                    Id = 2,
                    Name = "Travel",
                    CreatedAt = DateTime.UtcNow,
                    Description = "Articles about traveling",
                },
                new Category
                {
                    Id = 3,
                    Name = "Photography",
                    CreatedAt = DateTime.UtcNow,
                    Description = "Articles about photography",
                },
            });
        }
    }
}
