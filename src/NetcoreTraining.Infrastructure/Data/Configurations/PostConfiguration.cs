﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetcoreTraining.Core.Entities;
using System;

namespace NetcoreTraining.Infrastructure.Data.Configurations
{
    internal class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(entity => entity.Id);

            builder.Property(entity => entity.CreatedAt)
                .HasDefaultValueSql("GETDATE()");

            builder.Property(entity => entity.IsDeleted)
                .HasDefaultValue(false);

            builder.HasOne(post => post.Category)
                .WithMany(category => category.Posts)
                .HasForeignKey(post => post.CategoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(post => post.CategoryId);

            builder.Property(post => post.Title)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(post => post.ShortDescription)
                .HasMaxLength(500);

            builder.HasData(new Post[]
            {
                new Post
                {
                    Id = 1,
                    CategoryId = 1,
                    Title = "Lorem ipsum dolor sit amet",
                    CreatedAt = DateTime.UtcNow,
                    ShortDescription = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.",
                    Thumbnail = "/images/post-thumb-1.jpg",
                    Content = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.",
                },
                new Post
                {
                    Id = 2,
                    CategoryId = 2,
                    Title = "consetetur sadipscing elitr",
                    CreatedAt = DateTime.UtcNow,
                    ShortDescription = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.",
                    Thumbnail = "/images/post-thumb-1.jpg",
                    Content = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.",
                },
                new Post
                {
                    Id = 3,
                    CategoryId = 3,
                    Title = "sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat",
                    CreatedAt = DateTime.UtcNow,
                    ShortDescription = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.",
                    Thumbnail = "/images/post-thumb-1.jpg",
                    Content = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirtempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.At vero accusam et justo duo dolores et ea rebum.Stet clita kasd gubergren, no sea takimata sanctusest Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elised diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sdiam voluptua.At vero eos et accusam.",
                },
            });
        }
    }
}
