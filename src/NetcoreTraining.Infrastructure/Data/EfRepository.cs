﻿using Microsoft.EntityFrameworkCore;
using NetcoreTraining.Core.Entities;
using NetcoreTraining.Core.Enums;
using NetcoreTraining.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NetcoreTraining.Infrastructure.Data
{
    public class EfRepository<T> : IRepository<T> where T : EntityBase
    {

        protected readonly ApplicationDbContext _dbContext;

        public EfRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbContext.Set<T>()
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetByCriteriaAsync(Expression<Func<T, bool>> criteria = null, Expression<Func<T, object>> orderBy = null,
            SortOrder sort = SortOrder.Ascending, int take = -1, int skip = -1)
        {
            var result = _dbContext.Set<T>().Where(criteria ?? (T => true));

            orderBy = orderBy ?? (T => T.Id);
            result = sort == SortOrder.Ascending ? result.OrderBy(orderBy) : result = result.OrderByDescending(orderBy);

            if (skip != -1)
            {
                result = result.Skip(skip);
            }

            if (take != -1)
            {
                result = result.Take(take);
            }

            return await result.ToListAsync();
        }

        public async Task<T> GetByIdAsync(int Id)
        {
            return await _dbContext.Set<T>()
                .FindAsync(Id);
        }

        public async Task<int> CountAsync()
        {
            return await _dbContext.Set<T>()
                .CountAsync();
        }

        public async Task<int> CountByCriteriaAsync(Expression<Func<T, bool>> criteria)
        {
            return await _dbContext.Set<T>()
                .CountAsync(criteria);
        }
    }
}
