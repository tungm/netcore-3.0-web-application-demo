﻿using Microsoft.AspNetCore.Mvc;
using NetcoreTraining.Core.Interfaces.Sevices;
using NetcoreTraining.Web.ViewModels;
using NetcoreTraining.Web.ViewModels.Post;
using System.Linq;
using System.Threading.Tasks;

namespace NetcoreTraining.Web.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        public async Task<ActionResult> ListPost()
        {
            var posts = await _postService.ListPostByPageAsync(2, 1);

            var model = posts.Select(o => new PostListingViewModel
            {
                Id = o.Id,
                Title = o.Title,
                Thumbnail = o.Thumbnail,
                ShortDescription = o.ShortDescription,
                Category = new CategoryViewModel
                {
                    Id = o.Category.Id,
                    Name = o.Category.Name,
                },
            });

            return View(model);
        }

        [Route("{id}")]
        [Route("Post/{id}")]
        public async Task<ActionResult> Details(int id)
        {
            var post = await _postService.GetPostDetailById(id);

            if (post == null)
            {
                return RedirectToAction(nameof(ListPost));
            }

            var model = new PostDetailsViewModel
            {
                Title = post.Title,
                Thumbnail = post.Thumbnail,
                ShortDescription = post.ShortDescription,
                Category = new CategoryViewModel
                {
                    Id = post.Category.Id,
                    Name = post.Category.Name
                },
                Content = post.Content,
            };

            return View(model);
        }
    }
}