﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetcoreTraining.Web.ViewModels.Post
{
    public class PostDetailsViewModel
    {
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public CategoryViewModel Category { get; set; }
    }
}
