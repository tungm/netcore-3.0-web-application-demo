﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetcoreTraining.Web.ViewModels;
using NetcoreTraining.Core.Entities;
using NetcoreTraining.Core.Interfaces.Repositories;

namespace NetcoreTraining.Web.Components
{
    public class HeaderCategoriesViewComponent : ViewComponent
    {
        private readonly IRepository<Category> _categoryRepository;
        public HeaderCategoriesViewComponent(IRepository<Category> categoryRepository) => _categoryRepository = categoryRepository;

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = await _categoryRepository.GetAllAsync();
            var model = categories.Select(o => new CategoryViewModel
            {
                Id = o.Id,
                Name = o.Name
            });

            return View(model);
        }
    }
}
